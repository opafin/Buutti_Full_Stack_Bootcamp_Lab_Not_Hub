function factorial(n: number): number {
  if (n > 1) return n * factorial(n - 1)
  else {
    return n
  }
  // tässä else on oikeastaan turha, koska return päättää funktion jos sinne mennään
}
console.log(factorial(5))
