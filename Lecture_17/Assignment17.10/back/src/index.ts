import express, { Request, Response } from 'express'
import songs from '../../src/songs'
const app = express()

app.use(express.json())
app.use(express.static('./dist/client/'))

app.get('/songs', (req: Request, res: Response) => {
  const titlesAndIds = songs.map((song) => {
    const parsedSong = {
      id: song.id,
      title: song.title
    }
    return parsedSong
  })
  res.send(titlesAndIds)
})

app.get('/songs/:id', (req: Request, res: Response) => {
  const id = req.params.id
  const song = songs.find((song) => song.id === Number(id))
  res.send(song)
})

app.listen(3000, () => {
  console.log('Server is listening on port 3000!')
})
