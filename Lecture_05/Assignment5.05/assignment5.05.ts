import { tomaatti } from '../Assignment5.04/assignment5.04'
import { jauheliha } from '../Assignment5.04/assignment5.04'
import { Recipe, Ingredient } from '../Assignment5.04/assignment5.04'

interface HotRecipe extends Recipe {
  pepperRating: number
}
class HotRecipe extends Recipe {
  heatLevel: number
  constructor(name: string, ingredients: Ingredient[], amountOfDishes: number, pepperRating: number) {
    super(name, ingredients, amountOfDishes)
    this.heatLevel = pepperRating
  }
  HottoString() { // metodien nimet pienellä
    if (this.heatLevel > 5) return `'WOOOOOH' + ${this.heatLevel}`
  }
  // tää ei nyt overridaa toString metodia, koska sillä on eri nimi
  // HotRecipellä on nyt kaksi metodia: .toString ja .HottoString
}
const fierylihapullasoppa = new HotRecipe('fierylihapullasoppa', [jauheliha, tomaatti], 5, 10)
console.log(fierylihapullasoppa.HottoString())

// lihapullakeitto (50 servings)

// - jauheliha (50)
// - tomaatti (50)

// 'WOOOOOH' + 10
